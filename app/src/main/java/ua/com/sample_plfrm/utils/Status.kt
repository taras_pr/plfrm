package ua.com.sample_plfrm.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}