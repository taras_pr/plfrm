package ua.com.sample_plfrm.data.api

import retrofit2.Response
import ua.com.sample_plfrm.data.model.TodoInfo
import javax.inject.Inject

class AppApiHelper  @Inject constructor(private val apiService: ApiService): ApiHelper {

    override suspend fun getTodoInfo(id: String): Response<TodoInfo> = apiService.getTodoInfo(id)

}