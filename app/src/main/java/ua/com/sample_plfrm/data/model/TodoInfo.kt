package ua.com.sample_plfrm.data.model

import com.squareup.moshi.Json

data class TodoInfo (
    @Json(name = "userId")
    val userId: Int = 0,
    @Json(name = "id")
    val id: Int = 0,
    @Json(name = "title")
    val title: String = "",
    @Json(name = "completed")
    val completed: Boolean = false
){
    public fun getImage(): String? {
        val PATH_COMPLETED = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Five-pointed_star.svg/1200px-Five-pointed_star.svg.png"
        val PATH_INCOMPLETED = "https://solarsystem.nasa.gov/system/basic_html_elements/11561_Sun.png"

        return if (completed)
            PATH_COMPLETED else PATH_INCOMPLETED
    }
}