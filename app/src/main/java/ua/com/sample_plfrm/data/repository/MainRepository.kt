package ua.com.sample_plfrm.data.repository

import ua.com.sample_plfrm.data.api.ApiHelper
import javax.inject.Inject

class MainRepository @Inject constructor(private val apiHelper: ApiHelper){

    suspend fun getTodoInfo(id : String) = apiHelper.getTodoInfo(id)

}