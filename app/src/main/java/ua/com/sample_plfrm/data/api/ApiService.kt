package ua.com.sample_plfrm.data.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import ua.com.sample_plfrm.data.model.TodoInfo

interface ApiService {

    @GET("/todos/{id}")
    suspend fun getTodoInfo(@Path("id") id:String): Response<TodoInfo>

}