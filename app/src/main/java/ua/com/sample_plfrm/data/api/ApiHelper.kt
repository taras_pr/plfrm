package ua.com.sample_plfrm.data.api

import retrofit2.Response
import ua.com.sample_plfrm.data.model.TodoInfo

interface ApiHelper {

    suspend fun getTodoInfo(id : String): Response<TodoInfo>

}