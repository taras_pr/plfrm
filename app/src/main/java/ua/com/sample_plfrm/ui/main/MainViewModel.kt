package ua.com.sample_plfrm.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import ua.com.sample_plfrm.data.model.TodoInfo
import ua.com.sample_plfrm.data.repository.MainRepository
import ua.com.sample_plfrm.utils.NetworkHelper
import ua.com.sample_plfrm.utils.Resource
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository, private val networkHelper: NetworkHelper) : ViewModel() {

    private val countLive = MutableLiveData<Int>()
    private val todoInfo = MutableLiveData<Resource<TodoInfo>>()

    init {
        countLive.value = 0
    }

    public fun getCount() = countLive as LiveData<Int>

    public fun getTodoInfo() = todoInfo as LiveData<Resource<TodoInfo>>

    fun setCount(count:Int){
        countLive.value = count
    }

    public fun fetchTodoInfo() {
        viewModelScope.launch {
            todoInfo.postValue(Resource.loading(null))
            if (networkHelper.isNetworkConnected()) {
                mainRepository.getTodoInfo(getCount().value.toString()).let {
                    if (it.isSuccessful) {
                        todoInfo.postValue(Resource.success(it.body()))
                    } else todoInfo.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else todoInfo.postValue(Resource.error("No internet connection", null))
        }
    }
}