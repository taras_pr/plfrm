package ua.com.sample_plfrm.ui.launcher

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_launcher.*
import ua.com.sample_plfrm.R
import ua.com.sample_plfrm.ui.main.MainActivity

class LauncherActivity : AppCompatActivity() {

    private var count = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)

        startButton.setOnClickListener { openMainActivity() }

    }

    private fun openMainActivity() {
        startActivity(Intent(this, MainActivity::class.java).apply {
            putExtra(ARG_COUNT, ++count)
        })
    }

    companion object {

        public val ARG_COUNT = "ARG_COUNT"

    }
}