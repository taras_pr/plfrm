package ua.com.sample_plfrm.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import dagger.hilt.android.AndroidEntryPoint
import ua.com.sample_plfrm.R
import ua.com.sample_plfrm.databinding.ActivityMainBinding
import ua.com.sample_plfrm.ui.launcher.LauncherActivity.Companion.ARG_COUNT
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var activityMain: ActivityMainBinding

    private val mainViewModel:MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMain = DataBindingUtil.setContentView(this, R.layout.activity_main)

        initObserver()

        val count = intent.getIntExtra(ARG_COUNT, 0)
        mainViewModel.setCount(count)

        mainViewModel.fetchTodoInfo()
    }

    private fun initObserver() {
        activityMain.run {
            viewModel = mainViewModel
            lifecycleOwner = this@MainActivity
        }
    }
}